# -*- coding: utf-8 -*-
"""
Created on Mon Feb  6 07:07:19 2023

@author: paulo
"""

from tkinter import *
from tkinter import ttk
from PIL import Image, ImageTk
import os
    
# Janela principal
janela = Tk()
janela.resizable(0,0)
janela.title("Salta-Martim")
janela.geometry("348x161")


# Frame para imagem
frm_img = Frame(janela, width=59, height=48)
frm_img.place(x=11, y=30)

# Configuração do frame da imagem
imagem = Image.open("vagalume.png")
imagem = imagem.resize((59,48))
imagem = ImageTk.PhotoImage(imagem)

# Label para imagem
lbl_imagem = Label(frm_img, image=imagem, compound=RIGHT)
lbl_imagem.pack()

# Label para texto
lbl_txt = Label(janela, width=28, height=1, fg="#00FFFF", bg="black", font=20, text="Desligar este sistema agora?")
lbl_txt.place(x=73, y=46)

# Frame para os botões
frm_botoes = Frame(janela, width=348, height=29)
frm_botoes.place(x=0, y=129)

# Imagem para botão desligar
foto_desligar = PhotoImage(file="desligar.png")
fotoimage1 = foto_desligar.subsample(1,1)

# Imagem para botão reiniciar
foto_reiniciar = PhotoImage(file="reiniciar.png")
fotoimage2 = foto_reiniciar.subsample(1,1)

# Imagem para botão logout
foto_logout = PhotoImage(file="cancelar.png")
fotoimage3 = foto_logout.subsample(1,1)

#Funções
def desligar():
    return os.system("shutdown /s /t 1")

def reiniciar():
    return os.system("shutdown /r /t 1")

def sair():
    return os.system("shutdown -l")

# Botões
# Desligar
btdesligar = Button(frm_botoes,
                    fg="#00FFFF",
                    text="Desligar",
                    image=fotoimage1,
                    compound=LEFT,
                    command=desligar)
btdesligar.pack(side=LEFT)

# Reiniciar
btreiniciar = Button(frm_botoes,
                     fg="#00FFFF",
                     text="Reiniciar",
                     image=fotoimage2,
                     compound=LEFT,
                     command=reiniciar)
btreiniciar.pack(side=LEFT)

# Logout
btlogout = Button(frm_botoes,
                  fg="#00FFFF",
                  text="Logout",
                  width=93,
                  image=fotoimage3,
                  compound=LEFT,
                  command=sair)
btlogout.pack(side=LEFT)



janela.mainloop()
